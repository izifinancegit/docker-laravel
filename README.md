# docker-laravel

  

## como instalar o ambiente de desenvolvimento para novas aplicações

primeiro deve instalar o docker e o docker-tool-box
voce tera que criar uma conta no docker para poder fazer o download

[Docker](https://store.docker.com/editions/community/docker-ce-desktop-mac)        
[Docker Tool Box](https://docs.docker.com/toolbox/toolbox_install_mac/)

depois trocar os nomes dentro do **docker-compose.yml** (isso deve ser automatizado)

`container_name: nomePrj-mysql`

no caso trocar o **nomePrj** pelo nome do projeto, por exemplo se o projeto for **izi-engine** ficaria

`container_name: izi-engine-mysql`
(fazer isso em todos os containers no .yml)

---------------

## configurar o banco de dados

    mysql:
	    image: mysql
	    command: --default-authentication-plugin=mysql_native_password
	    ports:
		    - "4306:3306"
	    container_name: nomePrj-mysql
	    restart: always
	    environment:
		    MYSQL_ROOT_PASSWORD: 123456 
		    MYSQL_DATABASE: databseName

no caso so trocar o nome do banco de dados para o do projeto no campo **MYSQL_DATABASE: databseName**

-------------------

## Clonar o projeto

voce tera que clona o projeto da seguinte forma 

` git clone  https://github.com/nomeUsuario/dockerLaravelExemplo.git src`

esse **src** e para clonar o projeto em uma pasta com esse nome, mas por que esse src? é porque ja esta configurado dentro do docker-compose.yml o caminho do projeto.

## Subindo o projeto
	
depois disso só dar a permissão de execução para o config.sh

	chmod 777 config.sh

depois disso e so executar o config.sh que ira subir as imagens e o projeto vai estar rodando no **localhost:8080**

    ./config.sh